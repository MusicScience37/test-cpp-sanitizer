# test-cpp-sanitizer

C++ の Sanitizer の動作確認用プログラム

## 必要なもの

- Clang コンパイラ
- CMake

## ビルド手順

```bash
cd <リポジトリのルートディレクトリ>
mkdir build
cd build
cmake ..
cmake --build .
```
